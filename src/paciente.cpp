#include "paciente.hpp"

Paciente::Paciente(){

}

Paciente::Paciente(string nome, int idade, string sexo, class Medico *medico, int quarto){
	this->nome = nome;
	this->idade = idade;
	this->sexo = sexo;
	this->medico = medico;
	this->quarto = quarto;
}

void Paciente::setMedico(class Medico *medico){
	this->medico = medico;
}

void Paciente::setQuarto(int quarto){
	this->quarto = quarto;
}

class Medico Paciente::getMedico(){
	return *medico;
}

int Paciente::getQuarto(){
	return quarto;
}
