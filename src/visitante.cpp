#include "visitante.hpp"

Visitante::Visitante(){

}

Visitante::Visitante(string nome, int idade, string sexo, class Paciente *paciente, int quarto){
	this->nome = nome;
	this->idade = idade;
	this->sexo = sexo;
	this->paciente = paciente;
	this->quarto = quarto;
}

void Visitante::setPaciente(class Paciente *paciente){
	this->paciente = paciente;
}

void Visitante::setQuarto(int quarto){
	this->quarto = quarto;
}

class Paciente Visitante::getPaciente(){
	return *paciente;
}

int Visitante::Visitante::getQuarto(){
	return quarto;
}
