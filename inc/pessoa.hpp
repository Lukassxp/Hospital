#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <iostream>
#include <string>

using namespace std;

class Pessoa{
	protected:
		string nome;
		int idade;
		string sexo;

	public:
		Pessoa();
		Pessoa(string nome, int idade, string sexo);
		void setNome(string nome);
		void setIdade(int idade);
		void setSexo(string sexo);
		string getNome();
		int getIdade();
		string getSexo();
};

#endif
