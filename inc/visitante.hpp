#ifndef VISITANTE_HPP
#define VISITANTE_HPP

#include "pessoa.hpp"
#include "paciente.hpp"

#include <iostream>
#include <string>

using namespace std;

class Visitante : public Pessoa{
	public:
		class Paciente *paciente;
		int quarto;

	
		Visitante();
		Visitante(string nome, int idade, string sexo,class Paciente *paciente, int quarto);
		void setPaciente(class Paciente *paciente);
		void setQuarto(int quarto);
		class Paciente getPaciente();
		int getQuarto();
};

#endif
