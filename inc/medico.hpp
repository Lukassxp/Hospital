#ifndef MEDICO_HPP
#define MEDICO_HPP

#include "pessoa.hpp"
#include "paciente.hpp"

#include <iostream>
#include <string>

using namespace std;

class Medico : public Pessoa{
	public:
		class Paciente *paciente;
		string especialidade;

	
		Medico();
		Medico(string nome, int idade, string sexo,class Paciente *paciente, string especialidade);
		void setPaciente(class Paciente *paciente);
		void setEspecialidade(string especialidade);
		class Paciente getPaciente();
		string getEspecialidade();
};

#endif
