#ifndef PACIENTE_HPP
#define PACIENTE_HPP

#include "pessoa.hpp"
#include "medico.hpp"

#include <iostream>
#include <string>

using namespace std;

class Paciente : public Pessoa{
	public:
		class Medico *medico;
		int quarto;

	
		Paciente();
		Paciente(string nome, int idade, string sexo, class Medico *medico, int quarto);
		void setMedico(class Medico *medico);
		void setQuarto(int quarto);
		class Medico getMedico();
		int getQuarto();
};

#endif
