# Hospital - OO (UnB - Gama)

Este projeto consiste em um exercício em C++ da aula de orientação a objetos.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

-Dê um git clone no repositório

-Acesse a pasta pelo terminal

-Realize o comando "make clean" no terminal para limpar os arquivos .o e .bin

-Realize o comando "make" no terminal para compilar

-Realize o comando "make run" no terminal para rodar o programa
